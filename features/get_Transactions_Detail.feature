@get_Transactions_Detail
Feature: Get Transactions Detail
	As an API consumer
	I want to query charge requests
	So that I know they have been processed		

	@get-Transactions-Detail_byValidRefNumber_Sale
    Scenario: Verify the API get_Transactions_Detail with Sale ReferenceNumber
        Given I have valid merchant credentials
		When I perform a Sale transaction		
		And I get that Same Transaction
	#	When I use HMAC and GET /transactions/`reference`
        Then response code should be 200
        And response body should contain "reference":`reference`	
		And response body should contain "type":"Sale"
		And response body should contain "service":"Bankcard"
		
	@get_Transactions_Detail-byValidRefNumber_Force
    Scenario: Verify the API get_Transactions_Detail with Force ReferenceNumber
        Given I have valid merchant credentials
		When I perform a Force transaction		
		And I get that Same Transaction
        Then response code should be 200
        And response body should contain "reference":`reference`
		And response body should contain "type":"Force"
		And response body should contain "service":"Bankcard"
		
	@get_Transactions_Detail-byValidRefNumber_Auth
    Scenario: Verify the API get_Transactions_Detail with Authorization ReferenceNumber
        Given I have valid merchant credentials
		When I perform a Authorization transaction		
		And I get that Same Transaction
        Then response code should be 200
        And response body should contain "reference":`reference`
		And response body should contain "type":"Authorization"
		And response body should contain "service":"Bankcard"
		
	@get_Transactions_Detail-byValidRefNumber_Credit
    Scenario: Verify the API get_Transactions_Detail with Credit ReferenceNumber
        Given I have valid merchant credentials
		When I perform Valid Credit transaction
		And I get that Same credit Transaction
        Then response code should be 200
        And response body should contain "reference":`reference`
		And response body should contain "type":"Credit"
		And response body should contain "service":"Bankcard"
		
		
   #	************ Negative Scenarios ***********************
		
	# Blank ReferenceNumber we can't create for get_Transactions_Detail
	  
	@get_Transactions_Detail_InvalidReferenceNumber
    Scenario: Verify the API get_Transactions_Detail with invalid ReferenceNumber
        Given I have valid merchant credentials
	#	When I FETCH the Transaction Details with InValid Reference Number = abcabcabc
		When I use HMAC and GET /transactions/abcabcabc
        Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
		
		
		
		
		
		
		