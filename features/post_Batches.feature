@Post_Batches
Feature: settle the current set of transaction
	As an API consumer
	I want to settle the current set of transaction
	So that I know this flow works correctly

	@batch-Settle
	Scenario: Perform a transaction and then settle the current set of transactions 
		Given I have valid merchant credentials
	#	And I create a sale transaction
		And I perform a Sale transaction
		When I settle all current set of transaction
		Then response code should be 201
		And response body path $.message should be BATCH 1 OF 1 CLOSED
		
	@batch-settleNoTransaction
	Scenario: Try to Settle when there is no open transaction
		Given I have valid merchant credentials
		When I settle all current set of transaction
		Then response code should be 200
		And response body path $.message should be NO TRANSACTIONS
		
		
