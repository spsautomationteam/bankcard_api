@Get_Batches_Reference
Feature: Get_Batches_Reference
	As an API consumer
	I want to query batches requests
	So that I know they have been processed

#	@get_Batches_Reference_AllBatchTransactions
#    Scenario: query to get all Get Batches Current transactions
#       Given I have valid merchant credentials
#		When I perform Valid Post Batches Current transaction
#		And I set body to { "settlementType": "Bankcard",     "count": 10,     "net": 10,     "terminalNumber": "" }
#		When I use HMAC and POST to /batches/current
#		And I get List of Existing Current Batche Transactions
#       Then response code should be 200
#       And response header Content-Type should be application/json
#		And response body path should contain above performed transaction reference	
#		And response body should contain "reference"
#		And response body should contain "service"
#		And response body should contain "billing"
#		And response body should contain "shipping"
#		And response body should contain "customer"
#		And response body should contain "paymentType"	


	@get-Batches-Reference_byPostBachCurrentReference
    Scenario: Verify the API Get batches with Post BachCurrent Reference
        Given I have valid merchant credentials
		And I perform a Sale transaction
		When I settle all current set of transaction
		Then response code should be 201			
		When I get List of Transactions for an Existing Batch
		Then response code should be 200
		
	@get-Batches-Reference_byPageSize
    Scenario: Verify the API Get batches with Page Size
        Given I have valid merchant credentials
		And I perform a Sale transaction
		When I settle all current set of transaction
		Then response code should be 201			
		When I tried to GET Batches with Valid Reference and PageSize = 2
		Then response code should be 200
		And response body should contain "pageSize":2		
			
	@get-Batches-Reference_byPageNumber
    Scenario: Verify the API Get batches with Page Number
        Given I have valid merchant credentials
		And I perform a Sale transaction
		When I settle all current set of transaction
		Then response code should be 201			
		When I tried to GET Batches with Valid Reference and PageNumber = 1
	#	When I use HMAC and GET /batches/`BatchReference`?PageNumber=1
		Then response code should be 200
		And response body should contain "pageNumber":1		
		
		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++	
		
		
#	Can't Create Negative Scenario for Page Number

#	@get-Batches-Reference_byinValidPageNumber
#     Scenario: Verify the API Get batches with inValidPageNumber
#        Given I have valid merchant credentials
#		When I use HMAC and GET /batches/current?PageNumber=ABC
#       Then response code should be 200
#       And response body should not contain "pageNumber":ABC
#		And response body should not contain "reference"
#		And response body should not contain "service"
#		And response body should not contain "billing"
#		And response body should not contain "shipping"
#		And response body should not contain "customer"
#		And response body should not contain "paymentType"	  

#	Can't Create Negative Scenario for PageSize	
	  
#	 @get-Batches-Reference_byinValidPageSize
#     Scenario: Verify the API Get batches with inValidPageSize
#       Given I have valid merchant credentials
#		When I use HMAC and GET /batches/current?PageSize=FSFSDFSDFSFF
#       Then response code should be 200
#       And response body should not contain "pageSize":FSFSDFSDFSFF	
#		And response body should not contain "reference"
#		And response body should not contain "service"
#		And response body should not contain "billing"
#		And response body should not contain "shipping"
#		And response body should not contain "customer"
#		And response body should not contain "paymentType" 

	@get_Batches_Reference_byInvalidReferenceNumber
    Scenario: Verify the API Get Batches Reference with Invalid Reference Number
        Given I have valid merchant credentials
        When I GETS the Batche transaction with InValid Reference Number = abcabcabc
		Then response code should be 503
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
	
	
	
   