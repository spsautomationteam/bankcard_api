@get_Charges_Detail
Feature: Charges
	As an API consumer
	I want to query charge requests
	So that I know they have been processed		
		
	@get_Charges_Detail-Sale-byReferenceNumber
    Scenario: Verify the API Get charges with Sale ReferenceNumber
        Given I have valid merchant credentials
		And I perform a Sale transaction		     
		When I get that same charge
        Then response code should be 200
        And response body should contain "reference":`reference`
		
	# Blank ReferenceNumber we can't create for get_Charges_Detail
	
	############### Negative Scenarios ###################
	  
	@get_Charges_Detail_InvalidReferenceNumber
    Scenario: Verify the API get_Charges_Detail with invalid ReferenceNumber
        Given I have valid merchant credentials  
		When I use HMAC and GET /charges/ABCABCABC
        Then response code should be 404
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
		
		
		
		
		
		
		