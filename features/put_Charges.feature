@put_Charges
Feature: Authorization and Capture
	As an API consumer
	I want to create an authorization charge and then capture
	So that I know this flow works correctly

	@put_Charges-byAuthTransation
	Scenario: Post an Authorization Transaction and Capture the transaction
		Given I have valid merchant credentials	
		And I perform a Authorization Transaction
		And I Capture the Authorization transaction with Total Amount 10
		And I get that same charge
		Then response code should be 200
        And response body path $.amounts.total should be 10
		
	
	#	************************ Negative Scenario ***********************
	
	@put_Charges-byAuthorizationTransation_Twice
	Scenario: Post an Authorization Transaction and tyr to Capture the transaction Twice
		Given I have valid merchant credentials	
		And I perform a Authorization Transaction
		And I Capture the Authorization transaction with Total Amount 10
		And Again try to Capture the Same transaction
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.

	@put_Charges-bySaleTransation
	Scenario: Post an Sale Transaction and Capture the transaction
		Given I have valid merchant credentials	
		And I perform a Sale Transaction
		And I Capture the Sale transaction with Total Amount 70
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@put_Charges-byForceTransation
	Scenario: Post an Force Transaction and Capture the transaction
		Given I have valid merchant credentials	
		And I perform a Force Transaction
		And I Capture the Force transaction with Total Amount 70
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@put_Charges-byInvalidReferenceNumber
    Scenario: Verify the API Put_Charges with Invalid Reference Number
 		Given I set clientId header to `clientId`
        And I set apikey header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		And I set authorization header to `authorization` 
		And Try to Capture the Transaction with Invalid Reference Number and Total Amount 60
		Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	@put_Charges-byBlankReferenceNumber
    Scenario: Verify the API Put_Charges with Blank Reference Number
        Given I have valid merchant credentials		
		And Try to Capture the Transaction with Blank Reference Number and Total Amount 50
		Then response code should be 401
        And response body path $.code should be 100005
		And response body path $.message should be Missing or invalid Application Identifier
		And response body path $.detail should be A valid Application ID is required in header parameter clientId.
		
		
		
		
		