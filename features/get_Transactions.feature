@Get_Transactions
Feature: Get Transactions
	As an API consumer
	I want to query Transactions requests

	@get-Transactions_byAllTypesReference
    Scenario Outline: query a specific charge
		Given I have valid merchant credentials
		When I perform a <TypeOfTransaction> transaction
		Then response code should be 201
		When I get a list of existing <TypeOfTransaction> transactions done
	#	And I get that Same Transaction
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain above performed transaction reference
		And response body should contain "type":"<TypeOfTransaction>"
		And response body should contain "reference":`reference`
		And response body should contain "service":"Bankcard"
   
		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	Force				|
		|	Credit 		  		|		
		
	@get-Transactions_byType
    Scenario Outline: Verify the API Get transactions with TransactionCode
        Given I have valid merchant credentials
        When I use HMAC and GET /transactions?type=<TransactionType>
        Then response code should be 200
        And response body should contain "type":"<TransactionType>"
		And response body should contain "reference":`reference`
		And response body should contain "service":"Bankcard"
	#	And response body path $.items.[transactionCode] should be <TransactionType>  	
			
		Examples:
		|TransactionType  |
		|Sale			  |
		|Authorization    |
		|Force 			  |	
		|Credit 		  |	
		
	@get-Transactions_byStartDate
    Scenario: Verify the API Get transactions with StartDate
       Given I have valid merchant credentials
        When I use HMAC and GET /transactions?startDate=2017-05-17
        Then response code should be 200
        And response body should contain "startDate":"2017-05-17" 
		
	@get-Transactions_byEndDate
    Scenario: Verify the API Get transactions with EndDate
       Given I have valid merchant credentials
        When I use HMAC and GET /transactions?endDate=2017-05-22
        Then response code should be 200
        And response body should contain "endDate":"2017-05-22"
		
	@get-Transactions_byPageSize
    Scenario: Verify the API Get transactions with PageSize
       Given I have valid merchant credentials
        When I use HMAC and GET /transactions?PageSize=1
        Then response code should be 200
        And response body should contain "pageSize":1			
			
	@get-Transactions_byPageNumber
    Scenario: Verify the API Get transactions with PageNumber
        Given I have valid merchant credentials
        When I use HMAC and GET /transactions?pageNumber=2
        Then response code should be 200
        And response body should contain "pageNumber":2		
		
	@get-Transactions_byIsPurchaseCard
    Scenario Outline: Verify the API Get transactions with IsPurchaseCard
        Given I have valid merchant credentials
        When I use HMAC and GET /transactions?IsPurchaseCard=<IsPurchaseCard>
        Then response code should be 200
        And response body should contain "isPurchaseCard":<IsPurchaseCard>
		
		Examples:
		|IsPurchaseCard    |
	#	|true    		   |
		|false             |
		
	@get-Transactions_byGatewayID
    Scenario: Verify the API Get transactions with GatewayID
		Given I have valid merchant credentials
        When I use HMAC and GET /transactions?GatewayId=`merchantId`
        Then response code should be 200
		And response header Content-Type should be application/json
        And response body should contain "gatewayId":"`merchantId`"		
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"		

		
	@get-Transactions_byOrderNumber
    Scenario: Verify the API Get transactions with Bankcard OrderNumber
        Given I have valid merchant credentials
		When I perform a Sale transaction		
		And I get that Same Transaction
	#	When I use HMAC and GET /transactions?orderNumber=`orderNumber`
        Then response code should be 200
		And response header Content-Type should be application/json    
		And response body should contain "orderNumber":`orderNumber`
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
	
	@get-Transactions_byTotalAmount
    Scenario: Verify the API Get transactions with TotalAmount
        Given I have valid merchant credentials
		And I set content-type header to application/json	
	    When I perform a Sale transaction
        Then response code should be 201
        And response body path $.status should be Approved
      # And I use HMAC and GET /transactions?totalAmount=`Total`
	    And I get that Same Transaction
        Then response code should be 200
        And response header Content-Type should be application/json    
		And response body path $.items[0].amounts.total should be `Total`
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
	@get-Transactions_byApprovalCode
    Scenario: Verify the Get Transactions API with Bankcard 6 digit ApprovalCode
        Given I have valid merchant credentials
		When I perform a Sale transaction		
		And I get that Same Transaction
	#	When I use HMAC and GET /transactions?approvalCode=`TransCode`
        Then response code should be 200
		And response header Content-Type should be application/json    
		And response body should contain "responseCode":`responseCode`
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
	@get-Transactions_byStatus
    Scenario Outline: Verify the API Get transactions with Status
        Given I have valid merchant credentials
	#	When I perform a Sale transaction		
	#	And I get that Same Transaction
        When I use HMAC and GET /transactions?Status=<Status>
        Then response code should be 200     
		And response header Content-Type should be application/json 		
		And response body should contain "status":"<Status>"
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
		Examples:
		|Status  |
		|Declined|		
		|Batch   |
		|Settled |
	#	|Expired |
		
  # @get-Transactions_bySource
  # Scenario Outline: Verify the API Get transactions with Source
   #     Given I have valid merchant credentials
     #   When I use HMAC and GET /transactions?Source=<Source>
     #   Then response code should be 200
     #   And response body should contain "applicationName": "<Source>"		
	#I commneted below code because of no transactions done through Mobile 
	#	Examples:
	#	|Source    |
	#	|Mobile    |
	#	|Recurring |
		
	@get-Transactions_byAccountNumber
    Scenario: Verify the API Get transactions with AccountNumber
        Given I have valid merchant credentials
		When I perform a Sale transaction		
		And I get that Same Transaction 
      #  When I use HMAC and GET /transactions?AccountNumber=`accountNumber`
        Then response code should be 200
		And response header Content-Type should be application/json 
        And response body should contain "accountNumber":`number`	
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"		
		
		
		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++
		
	 @get-Transactions_byinValidReference
     Scenario: Verify the API Get transactions with inValidReference
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?Reference=abcabcabc
        Then response code should be 200
        And response body should not contain "reference":"abcabcabc"
		
	@get-Transactions_StartDate_AsFutureDate
     Scenario: Verify the API Get Transactions with Future StartDate
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?StartDate=2030-12-12
        Then response code should be 200
        And response body should contain "startDate":"2030-12-12"
		And response body should not contain "reference"
		And response body should not contain "service" 
		And response body should not contain "paymentType"
		And response body should not contain "saleCount"
		And response body should not contain "saleTotal"
		And response body should not contain "creditCount"
		And response body should not contain "creditTotal"
		And response body should not contain "totalCount"
		And response body should not contain "totalVolume"		
		
	@get-Transactions_EndDate_AsPastDate
     Scenario: Verify the API Get Transactions with Past EndDate
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body should not contain "reference"
		And response body should not contain "service" 
		And response body should not contain "paymentType"
		And response body should not contain "saleCount"
		And response body should not contain "saleTotal"
		And response body should not contain "creditCount"
		And response body should not contain "creditTotal"
		And response body should not contain "totalCount"
		And response body should not contain "totalVolume"
		
	@get-Transactions_byinValidGatewayID
     Scenario: Verify the API Get transactions with inValidGatewayId
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?gatewayId=fsfddssdfsdf
        Then response code should be 200
        And response body should not contain "gatewayId":"fsfddssdfsdf"
		
#	@get-Transactions_byinValidPageNumber
#     Scenario: Verify the API Get transactions with inValidPageNumber
#        Given I have valid merchant credentials
#		When I use HMAC and GET /transactions?PageNumber=ABC
#        Then response code should be 200
#       And response body should not contain "pageNumber":ABC
	  
	  
#	 @get-Transactions_byinValidPageSize
#     Scenario: Verify the API Get transactions with inValidPageSize
#        Given I have valid merchant credentials
#		When I use HMAC and GET /transactions?PageSize=ABCD
#        Then response code should be 200
#        And response body should not contain "pageSize":ABCD	  
		
	@get-Transactions_byinValidOrderNumber
     Scenario: Verify the API Get transactions with inValidOrderNumber
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?orderNumber=FSFFFDFSDFSDDFF
        Then response code should be 200
		And response body should not contain "orderNumber":"`FSFFFDFSDFSDDFF`"
	
	@get-Transactions_byinValidApprovalCode
     Scenario: Verify the API Get transactions with inValidApprovalCode
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?approvalCode=FSFFFDFSDFSDDFF
        Then response code should be 200
        And response body should not contain "responseCode":"`FSFFFDFSDFSDDFF`"
	
	@get-Transactions_byinValidTransactionCode
     Scenario: Verify the API Get transactions with inValidTransactionCode
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?TransactionCode=FSFFFDFSDFSDDFF
        Then response code should be 200
        And response body should not contain "transactionCode":"FSFFFDFSDFSDDFF"	
	
	@get-Transactions_byinValidStatus
     Scenario: Verify the API Get transactions with inValidStatus
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?Status=fkjsdklfdsjsfdf
        Then response code should be 200
        And response body should not contain "status":"fkjsdklfdsjsfdf"
	
	@get-Transactions_byinValidSource
     Scenario: Verify the API Get transactions with inValidSource
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?Source=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"	
	
	@get-Transactions_byinValidTotalAmount
     Scenario: Verify the API Get transactions with inValidTotalAmount
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?IsPurchaseCard=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"	
		
	 @get-Transactions_byinValidIsPurchaseCard
     Scenario: Verify the API Get transactions with inValidPurchaseCard
        Given I have valid merchant credentials
		When I use HMAC and GET /transactions?totalAmount=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"	
	
	