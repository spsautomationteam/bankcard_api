@get_Batches_Current
Feature: Get_Batches_Current
	As an API consumer
	I want to query batches requests
	So that I know they have been processed

	@get-Batches-Current_AllBatchTransactions
    Scenario: query to get all Get Batches Current transactions
       Given I have valid merchant credentials
	   And I perform a Sale transaction
	   Then response code should be 201	
		And I set body to { "settlementType": "Bankcard"}
		When I use HMAC and POST to /batches/current
		Then response code should be 201
		And I get List of Existing Current Batche Transactions
        Then response code should be 200
        And response header Content-Type should be application/json	
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "billing"
		And response body should contain "shipping"
		And response body should contain "customer"
		And response body should contain "paymentType"	
		
	@get-Batches-Current_byPageSize
    Scenario: Verify the API Get batches with PageSize
       Given I have valid merchant credentials
        When I use HMAC and GET /batches/current?PageSize=2
        Then response code should be 200
        And response body should contain "pageSize":2
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "billing"
		And response body should contain "shipping"
		And response body should contain "customer"
		And response body should contain "paymentType"
		And response body path $.items[1].service should be Bankcard 
			
	@get-Batches-Current_byPageNumber
    Scenario: Verify the API Get batches with PageNumber
         Given I have valid merchant credentials
        When I use HMAC and GET /batches/current?pageNumber=1
        Then response code should be 200
        And response body should contain "pageNumber":1
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "billing"
		And response body should contain "shipping"
		And response body should contain "customer"
		And response body should contain "paymentType"
			
	
   