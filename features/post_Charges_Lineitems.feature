@post-Charge-Lineitems
Feature: Post Charges Lineitems
	As an API consumer
	I want to create charge requests
	So that I know they can be processed
	
	@post-Charge-Lineitems_Details_byValidRefNumber_Sale
	Scenario: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I have valid merchant credentials
		When I perform a Sale transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
		And I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
		Then response code should be 201   		
		
	@post-Charge-Lineitems_Details_byValidRefNumber_Force
	Scenario: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I have valid merchant credentials
		When I perform a Force transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
		And I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
		Then response code should be 201   		
		
	@post-Charge-Lineitems_Details_byValidRefNumber_Authorization
	Scenario: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I have valid merchant credentials
		When I perform a Authorization transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"123"}}}
		And I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
		Then response code should be 201   		

	@post-Charge-Lineitems_MCard_byValidRefNumber
	Scenario: API post Charge Lineitems with Reference Number and MCard Lineitems Body
		Given I have valid merchant credentials
		When I perform a Sale transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"123"}}}
		And I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
        Then response code should be 201    
		
	@post-Charge-Lineitems_VisaCard_byValidRefNum
	Scenario: API post Charge Lineitems with Reference Number and VisaCard Lineitems Body
		Given I have valid merchant credentials
		When I perform a Sale transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
		And I use POST lineItems using lineItem Body {"visa": [{"commodityCode": "2","itemDescription": "dfgdfg","productCode": "5","quantity": 4,"unitOfMeasure": "0","unitCost": 40,"vatTaxAmount": 4,"vatTaxRate": 1,"discountAmount": 5,"lineItemTotal": 35}]}
        Then response code should be 201  
		
	
  ################  Negative Scenarios  ################

#	@post-Charge-Lineitems_MCard_byInValidRefNumber
#	Scenario: API post_charge_lineitems with invalid RefNumber
#		Given I have valid merchant credentials
#		And I use POST lineItems using Invalid RefNumber and Valid lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
#		Then response code should be 404
#		And response body path $.code should be 000000
#		And response body path $.message should be Internal Server Error
#		And response body path $.detail should be Please contact support for assistance.  
		
#	@post-Charge-Lineitems_VisaCard_byInValidRefNumber
#	Scenario: API post_charge_lineitems with InValid RefNumber
#		Given I have valid merchant credentials
#		When I use POST lineItems using Invalid RefNumber and lineItem Body {"visa": [{"commodityCode": "2","itemDescription": "dfgdfg","productCode": "5","quantity": 4,"unitOfMeasure": "0","unitCost": 40,"vatTaxAmount": 4,"vatTaxRate": 1,"discountAmount": 5,"lineItemTotal": 35}]}
#		Then response code should be 404
#		And response body path $.code should be 000000
#		And response body path $.message should be Internal Server Error
#		And response body path $.detail should be Please contact support for assistance.


	