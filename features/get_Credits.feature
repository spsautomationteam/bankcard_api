@get_Credits
Feature: Get Credits
	As an API consumer
	I want to query credits requests
	So that I know they have been processed

	@get_Credits_AllCreditTransactions
    Scenario: query all credits
        Given I have valid merchant credentials
		When I perform Valid Credit transaction
		And I get the list of credit transactions
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain above performed transaction reference		

	@get_Credits_byValidReferenceNumber
    Scenario: query a specific charge
		Given I have valid merchant credentials
		When I perform Valid Credit transaction
		And I get that Same credit Transaction
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain above performed transaction reference	
		
	@get-Credits_byStartDate
    Scenario: Verify the API Get credits with StartDate
        Given I have valid merchant credentials
        When I use HMAC and GET /credits?startDate=2017-05-17
        Then response code should be 200
        And response body should contain "startDate":"2017-05-17" 
		 And response header Content-Type should be application/json
		
	@get-Credits_byEndDate
    Scenario: Verify the API Get credits with EndDate
        Given I have valid merchant credentials
        When I use HMAC and GET /credits?endDate=2017-06-10
        Then response code should be 200
        And response body should contain "endDate":"2017-06-10"
		And response header Content-Type should be application/json
		
	@get-Credits_byPageSize
    Scenario: Verify the API Get credits with PageSize
        Given I have valid merchant credentials
        When I use HMAC and GET /credits?PageSize=1
        Then response code should be 200
        And response body should contain "pageSize":1
		And response header Content-Type should be application/json		
			
	@get-Credits_byPageNumber
    Scenario: Verify the API Get credits with PageNumber
        Given I have valid merchant credentials
        When I use HMAC and GET /credits?pageNumber=2
        Then response code should be 200
        And response body should contain "pageNumber":2		
		And response header Content-Type should be application/json
		
	@get-Credits_byIsPurchaseCard
    Scenario Outline: Verify the API Get credits with IsPurchaseCard
        Given I have valid merchant credentials
        When I use HMAC and GET /credits?IsPurchaseCard=<IsPurchaseCard>
        Then response code should be 200
        And response body should contain "isPurchaseCard":<IsPurchaseCard>
		And response header Content-Type should be application/json
		
		Examples:
		|IsPurchaseCard    |
	#	|true    		   |
		|false             |
		
	@get-Credits_byGatewayID
    Scenario: Verify the API Get credits with GatewayID
        Given I have valid merchant credentials   
		When I use HMAC and GET /credits?GatewayId=`merchantId`
        Then response code should be 200
        And response body should contain "gatewayId":"`merchantId`"
		
	@get-Credits_byReferenceNumber
    Scenario: Verify the API Get credits with Bankcard ReferenceNumber
        Given I have valid merchant credentials	
		When I perform Valid Credit transaction
		And I get that Same credit Transaction       
    #   When I use HMAC and GET /credits?Reference=`reference`
        Then response code should be 200
        And response body should contain "reference":`reference`

	@get-Credits_byOrderNumber
    Scenario: Verify the API Get credits with Bankcard OrderNumber
        Given I have valid merchant credentials
		When I perform Valid Credit transaction
		And I get that Same credit Transaction 	
	#	When I use HMAC and GET /credits?orderNumber=`orderNumber`
        Then response code should be 200
		And response body should contain "orderNumber":`orderNumber`		
	
	@get-Credits_byTotalAmount
    Scenario: Verify the API Get credits with TotalAmount
        Given I have valid merchant credentials	
		When I perform Valid Credit transaction
		Then response code should be 201
		And I get that Same credit Transaction 
        Then response code should be 200
        And response header Content-Type should be application/json    
		And response body path $.items[0].amounts.total should be `Total`
		
	@get-Credits_byApprovalCode
    Scenario: Verify the API Get credits with Bankcard ApprovalCode
        Given I have valid merchant credentials	
		When I perform Valid Credit transaction
		Then response code should be 201
		And I get that Same credit Transaction
		And I get the list of credit transactions
	#	When I use HMAC and GET /credits?approvalCode=`ResponseCode`	
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain "responseCode":`responseCode`
		
	@get-Credits_byStatus
    Scenario Outline: Verify the API Get credits with Status
       Given I have valid merchant credentials
        When I use HMAC and GET /credits?Status=<Status>
        Then response code should be 200        
		And response body should contain "status":"<Status>"
		
		Examples:
		|Status  |
		|Declined|		
		|Batch   |
		|Settled |
	#	|Expired |
		
	@get-Credits_byAccountNumber
    Scenario: Verify the API Get credits with AccountNumber
        Given I have valid merchant credentials
		And I use HMAC and GET /credits?accountNumber=1111
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain "accountNumber":"XXXXXXXXXXXX1111"								 		
		
		
		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++
		
	 @get-Credits_byinValidReference
     Scenario: Verify the API Get credits with inValidReference
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?Reference=ABCABC1234
        Then response code should be 200
        And response body should not contain "reference":"ABCABC1234"
		
	@get-Credits_StartDate_AsFutureDate
     Scenario: Verify the API Get Credits with Future StartDate
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?StartDate=2030-12-12
        Then response code should be 200
        And response body should contain "startDate":"2030-12-12" 	
		And response body should not contain "reference"
		And response body should not contain "service" 
		And response body should not contain "paymentType"
		And response body should not contain "saleCount"
		And response body should not contain "saleTotal"
		And response body should not contain "creditCount"
		And response body should not contain "creditTotal"
		And response body should not contain "totalCount"
		And response body should not contain "totalVolume"		  
		
	@get-Credits_EndDate_AsPastDate
     Scenario: Verify the API Get Credits with Past EndDate
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body should not contain "reference"
		And response body should not contain "service" 
		And response body should not contain "paymentType"
		And response body should not contain "saleCount"
		And response body should not contain "saleTotal"
		And response body should not contain "creditCount"
		And response body should not contain "creditTotal"
		And response body should not contain "totalCount"
		And response body should not contain "totalVolume"
		
	@get-Credits_byinValidGatewayID
     Scenario: Verify the API Get credits with inValidGatewayId
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?GatewayID=FSFSDFSDFSFF
        Then response code should be 200
        And response body should not contain "gatewayId":"FSFSDFSDFSFF"  
		
	@get-Credits_byinValidOrderNumber
     Scenario: Verify the API Get credits with inValidOrderNumber
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?orderNumber=FSFSDFSDFSFF
        Then response code should be 200
		And response body should not contain "orderNumber":"`FSFSDFSDFSFF`"
	
	@get-Credits_byinValidApprovalCode
     Scenario: Verify the API Get credits with inValidApprovalCode
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?approvalCode=FSFSDFSDFSFF
        Then response code should be 200
        And response body should not contain "responseCode":"`FSFSDFSDFSFF`"
	
	@get-Credits_byinValidTransactionCode
     Scenario: Verify the API Get credits with inValidTransactionCode
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?TransactionCode=FSFSDFSDFSFF
        Then response code should be 200
        And response body should not contain "transactionCode":"FSFSDFSDFSFF"	
	
	@get-Credits_byinValidStatus
     Scenario: Verify the API Get credits with inValidStatus
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?Status=fkjsdklfdsjsfdf
        Then response code should be 200
        And response body should not contain "status":"fkjsdklfdsjsfdf"
	
	@get-Credits_byinValidSource
     Scenario: Verify the API Get credits with inValidSource
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?Source=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"	
	
	@get-Credits_byinValidIsPurchaseCard
     Scenario: Verify the API Get credits with inValidPurchaseCard
        Given I have valid merchant credentials
		When I use HMAC and GET /credits?IsPurchaseCard=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"	
	
	
   