@get_Batches
Feature: Get Batches
	As an API consumer
	I want to query batches requests
	So that I know they have been processed

	@get_Batches_AllBatchTransactions
    Scenario: query to Get all Batch API Transactions
       Given I have valid merchant credentials	
		And I perform a Sale transaction
	#	And I set body to { "settlementType": "Bankcard",     "count": 10,     "net": 10,     "terminalNumber": "" }
		And I set body to { "settlementType": "Bankcard"}
		When I use HMAC and POST to /batches/current
		And I get List of Existing Batche Transactions
        Then response code should be 200
		And response header Content-Type should be application/json
	#	And response body path should contain above performed transaction reference	
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "terminalNumber"
		And response body should contain "count"
		And response body should contain "volume"			
		
	@get-Batches-byStartDate
    Scenario: Verify Get Batches API with StartDate
        Given I have valid merchant credentials
        When I use HMAC and GET /batches?startDate=2017-06-13
        Then response code should be 200
        And response body should contain "startDate":"2017-06-13" 
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "terminalNumber"
		And response body should contain "date"
		And response body should contain "count"
		And response body should contain "net"
		And response body should contain "volume"
		
	@get-Batches-byEndDate
    Scenario: Verify Get Batches API with EndDate
        Given I have valid merchant credentials
        When I use HMAC and GET /batches?endDate=2017-07-10
        Then response code should be 200
        And response body should contain "endDate":"2017-07-10"
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "terminalNumber"
		And response body should contain "date"
		And response body should contain "count"
		And response body should contain "net"
		And response body should contain "volume"
		
	@get-Batches-byPageSize
    Scenario: Verify the Get Batches API with PageSize and transaction Items count should match
       Given I have valid merchant credentials
        When I use HMAC and GET /batches?PageSize=2
        Then response code should be 200
        And response body should contain "pageSize":2
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "terminalNumber"
		And response body should contain "date"
		And response body should contain "count"
		And response body should contain "net"
		And response body should contain "volume"
		And response body path $.items[1].service should be Bankcard               
		
			
	@get-Batches-byPageNumber
    Scenario: Verify the Get batches API with PageNumber
        Given I have valid merchant credentials
        When I use HMAC and GET /batches?pageNumber=1
        Then response code should be 200
        And response body should contain "pageNumber":1
		And response body should contain "reference"
		And response body should contain "service"
		And response body should contain "terminalNumber"
		And response body should contain "date"
		And response body should contain "count"
		And response body should contain "net"
		And response body should contain "volume"
		
		
		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++	
	 
		
	@get-Batches-StartDate_AsFutureDate
     Scenario: Verify the API Get batches with Future StartDate
        Given I have valid merchant credentials
		When I use HMAC and GET /batches?StartDate=2030-12-12
        Then response code should be 200
        And response body should contain "startDate":"2030-12-12" 	
		And response body should not contain "reference"
		And response body should not contain "service"
		And response body should not contain "terminalNumber"
		And response body should not contain "date"
		And response body should not contain "count"
		And response body should not contain "net"
		And response body should not contain "volume"
		
	@get-Batches-EndDate_AsPastDate
     Scenario: Verify the API Get batches with Past EndDate
        Given I have valid merchant credentials
		When I use HMAC and GET /batches?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body should not contain "reference"
		And response body should not contain "service"
		And response body should not contain "terminalNumber"
		And response body should not contain "date"
		And response body should not contain "count"
		And response body should not contain "net"
		And response body should not contain "volume"		
		
#	Can't Create Negative Scenario for Page Number

#	@get-Batches-byinValidPageNumber
#     Scenario: Verify the API Get batches with inValidPageNumber
#        Given I have valid merchant credentials
#		When I use HMAC and GET /batches?PageNumber=ABC
#        Then response code should be 200
#       And response body should not contain "pageNumber":ABC	  

#	Can't Create Negative Scenario for PageSize	
	  
#	 @get-Batches-byinValidPageSize
#     Scenario: Verify the API Get batches with inValidPageSize
#       Given I have valid merchant credentials
#		When I use HMAC and GET /batches?PageSize=FSFSDFSDFSFF
#        Then response code should be 200
#        And response body should not contain "pageSize":FSFSDFSDFSFF	 
	
	
	
   