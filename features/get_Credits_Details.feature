@get_Credits_Detail
Feature: Get Credits Detail
	As an API consumer
	I want to query charge requests
	So that I know they have been processed		

	@get_Credits_Details-With_ValidReferenceNumber
    Scenario: Verify the API get_Credits_Detail with valid ReferenceNumber
        Given I have valid merchant credentials
		When I perform Valid Credit transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}	
		Then response code should be 201
		When I GETS the Credit transaction with Valid Reference Number
		Then response code should be 200
		And response body should contain "reference":`reference`	
		And response body should contain "type":"Credit"

	
#	**************** Negative Scenarios **************************		
		
	# Blank ReferenceNumber we can't create for get_Credits_Detail
	
	@get_Credits_Detail_byInvalidReferenceNumber
    Scenario: Verify the API Get_Credits Details with Invalid Reference Number
        Given I have valid merchant credentials
        When I GETS the Credit transaction with InValid Reference Number = abcabcabc	
		Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.  	
	  

		
		
		
		
		
		
		
		