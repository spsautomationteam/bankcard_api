/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

var Contenttype = "application/json";

var prettyJson = require('prettyjson');

var stepContext = {};

var getTransactionList = function( apickli, typeOfTransaction, callback ) {
		var pathSuffix = "/transactions?type=" + typeOfTransaction;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getTransaction = function( apickli, Reference, callback ) {
		var pathSuffix = "/transactions?reference=" +Reference;
		var url = apickli.domain + pathSuffix;
		
		console.log("pathSuffix : "+ pathSuffix);

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

module.exports = function () { 
	
	this.When(/^I get a list of existing (.*) transactions done$/, function (typeOfTransaction, callback) {      	
		getTransactionList(this.apickli, typeOfTransaction, callback);
	});	
	
	this.When(/^I get that Same Transaction$/, function (callback) {
        var Reference = this.apickli.scenarioVariables.Reference;
		getTransaction(this.apickli, Reference, callback);
	});
	
	this.When(/^I FETCH the Transaction Details with InValid Reference Number = (.*)$/, function (invRefNum, callback) {		
		console.log("Invalid Reference for Get Trans : " + invRefNum);
		getTransaction(this.apickli, invRefNum, callback);
	});

};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
