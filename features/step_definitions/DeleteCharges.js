/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createTx = function( apickli,TxType, callback ) {
		var pathSuffix = "/charges?type="+TxType;
		var url = apickli.domain + pathSuffix;
		
		var amount =Math.floor(Math.random() * 1000);		

		apickli.storeValueInScenarioScope("Total", amount);	
		
		//var randomVal = Math.random() * (101 - 999) + 101;
		//var body = { ECommerce: { Amounts: { Total: 1.0 },authorizationCode: 111222, CardData: { Expiration: 1216, Number: 4111111111111111 } } }
		var body = {     "transactionId": "",     "retail": {         "amounts": {             "tip": 0,             "total":amount,             "tax": 0,             "shipping": 0         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		var body = JSON.stringify(body);
		console.log("**********"+body);
		console.log("****amount******"+amount);
		// console.log("\n before replace  body >>>*: "+body);
		
		/*	var pat =/"total":\d+/g
			var randomVal ='"total":'+Math.floor(Math.random() * 10) + 1;
			console.log("\n  randomVal : "+randomVal);
			var body = body.replace(pat, randomVal);//'"total": 87');   */
		
		 //	console.log("\n after replace  body : "+body);		

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);
		//console.log("\n\n hmac :::::"+hmac)

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;
			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'OrderNumber');			
			console.log("Trans OrderNumber : "+ apickli.scenarioVariables.OrderNumber)	
			var OrderNumber = apickli.scenarioVariables.OrderNumber;
			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.code', 'TransCode');			
			console.log("Trans Code : "+ apickli.scenarioVariables.TransCode)	
			var OrderNumber = apickli.scenarioVariables.TransCode;
						
        	callback();     
		});
};

var createTxWithBody = function( apickli,TxType, TxBody, callback ) {
	
		var pathSuffix = "/charges?type="+TxType;
		var url = apickli.domain + pathSuffix;
		console.log("txbody : "+ TxBody);
		
		var body = TxBody;
		// var body = JSON.stringify(body);	
		
		//	var pat =/"total": \d+/g
		//	var randomVal ='"total": '+Math.floor(Math.random() * 10) + 1;
		//	console.log("\n  random Total : "+randomVal);
		//	var body = body.replace(pat, randomVal);//'"total": 87');
			//console.log("\n after replace  body : "+body);		

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);
		//console.log("\n\n hmac :::::"+hmac)

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;
			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'OrderNumber');			
			console.log("Trans OrderNumber : "+ apickli.scenarioVariables.OrderNumber)	
			var OrderNumber = apickli.scenarioVariables.OrderNumber;
						
        	callback();     
		});
};

var getSameTrans = function( apickli, Reference, callback ) {
		var pathSuffix = "/charges/" + Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var DeleteTx = function( apickli,pathSuffix,Reference, callback ) {
		
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "DELETE", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.delete(pathSuffix, callback);
		
};

module.exports = function () { 

	this.Given(/^I perform a (.*) transaction$/, function (TxType,callback) {
		createTx(this.apickli,TxType, callback);
	});
	
	this.Given(/^I perform a (.*) transaction with Body (.*)$/, function (TxType, TxBody, callback) {
		createTxWithBody(this.apickli,TxType, TxBody, callback);
	});
	
	this.When(/^I Delete the respective transaction by reference Number$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		console.log("Delete RefNumber : "+ Reference);
		var pathSuffix = "/charges/" + Reference;
		DeleteTx(this.apickli,pathSuffix,Reference,callback);
	});	
	
	this.When(/^Again try to perform same Sale transaction$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;		
		getSameTrans(this.apickli ,Reference, callback);
	});
	
	this.When(/^Again try to delete the same transaction$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		console.log("2nd Delete RefNumber : "+ Reference);
		var pathSuffix = "/charges/" + Reference;
		DeleteTx(this.apickli,pathSuffix,Reference,callback);
	});
	
	this.When(/^Verify Delete Charges with Invalid Reference Number$/, function (invalidRef, callback) {		
		// var invRefNum = "abcabcabc";
		var pathSuffix = "/charges/" + invalidRef;
		DeleteTx(this.apickli,pathSuffix,invalidRef,callback);
	});
	
	this.When(/^Verify Delete Charges with Blank Reference Number$/, function (callback) {		
		var blankRefNum = "";
		var pathSuffix = "/charges/" + blankRefNum;
		console.log("pathSuffix : "+pathSuffix)
		DeleteTx(this.apickli,pathSuffix,blankRefNum,callback);
		
	});	

	};
