/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createLineItemTrans = function(apickli, TxReference, callback ) {	

	var localApickli = factory.getNewApickliInstance(apickli.scenarioVariables.basepath);

	var pathSuffix = "/charges/"+TxReference+"/lineitems";
	var url = apickli.domain + pathSuffix;
	console.log( "URL ***** = "+url );
	var body = {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}

     var reqString =body;
        
	//var body = reqString; //JSON.stringify(reqString);
          var body = JSON.stringify(body);

      localApickli.setRequestBody(body);
       var nonce = hmacTools.nonce(12);
       var timestamp = Date.now() / 1000 + '';

        var clientSecret = apickli.scenarioVariables.clientSecret;
        var hmac =  hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);// "POST", url, body, '', nonce, timestamp);
        localApickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        localApickli.addRequestHeader('nonce', nonce);
        localApickli.addRequestHeader('timestamp', timestamp);
        localApickli.addRequestHeader('Authorization', hmac);
        localApickli.addRequestHeader('Content-Type', 'application/json');
        localApickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
		localApickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);

        localApickli.post(pathSuffix, function (err, response) {
        console.log(response.body);
                                          
        callback();//(response, apickli);
  });
};

var createLineItemTransWithBody = function(apickli, TxReference, lnbody, callback ) {	

	var localApickli = factory.getNewApickliInstance(apickli.scenarioVariables.basepath);

	var pathSuffix = "/charges/"+TxReference+"/lineitems";
	var url = apickli.domain + pathSuffix;
	console.log( "URL ***** = "+url );
	//var body = {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
     var body = lnbody;
     // var reqString =body;
	 console.log( "BODY ***** = "+lnbody );	 
        
	//var body = reqString; 
	 //JSON.stringify(reqString);
          //var body = JSON.stringify(body);

      localApickli.setRequestBody(body);
       var nonce = hmacTools.nonce(12);
       var timestamp = Date.now() / 1000 + '';

        var clientSecret = apickli.scenarioVariables.clientSecret;
        var hmac =  hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);// "POST", url, body, '', nonce, timestamp);
        localApickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        localApickli.addRequestHeader('nonce', nonce);
        localApickli.addRequestHeader('timestamp', timestamp);
        localApickli.addRequestHeader('Authorization', hmac);
        localApickli.addRequestHeader('Content-Type', 'application/json');
        localApickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
		localApickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);			

        localApickli.post(pathSuffix, function (err, response) {
        		
		if ( response ) {
				console.log( response.body);
			}
		
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("LineItem Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;		
		
        callback();//(response, apickli);
  });
};

var createLineItemTransWithInvRef = function(apickli, TxReference, lnbody, callback ) {	

	var localApickli = factory.getNewApickliInstance(apickli.scenarioVariables.basepath);

	var pathSuffix = "/charges/"+TxReference+"/lineitems";
	var url = apickli.domain + pathSuffix;
	console.log( "URL ***** = "+url );
	//var body = {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
     var body = lnbody;
     // var reqString =body;
	 console.log( "BODY ***** = "+lnbody );
        
	//var body = reqString; 
	 //JSON.stringify(reqString);
          //var body = JSON.stringify(body);

      localApickli.setRequestBody(body);
       var nonce = hmacTools.nonce(12);
       var timestamp = Date.now() / 1000 + '';

        var clientSecret = apickli.scenarioVariables.clientSecret;
        var hmac =  hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);// "POST", url, body, '', nonce, timestamp);
        localApickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        localApickli.addRequestHeader('nonce', nonce);
        localApickli.addRequestHeader('timestamp', timestamp);
        localApickli.addRequestHeader('Authorization', hmac);
        localApickli.addRequestHeader('Content-Type', 'application/json');
        localApickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
		localApickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);			

        localApickli.post(pathSuffix, function (err, response) {
        		
		if ( response ) {
				console.log( response.body);
			}		
		
        callback();//(response, apickli);
  });
};

var getChargesLineItem = function(apickli, TxReference, callback ) {
		
		var pathSuffix = "/charges/"+TxReference+"/lineitems";
		var url = apickli.domain + pathSuffix;
		
		console.log("pathSuffix : "+ pathSuffix);

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var deleteChargesLineItem = function(apickli, TxReference, callback ) {
	
		var pathSuffix = "/charges/"+TxReference+"/lineitems";				
		console.log("pathSuffix : "+ pathSuffix);
		
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "DELETE", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.delete(pathSuffix, callback);
		
};

module.exports = function () { 
	
	this.When(/^I use POST lineItems using lineItem (.*) Body$/, function (callback) {		
		
		var ReferenceNum = this.apickli.scenarioVariables.Reference;
		console.log("RefNumber for POST lineItems : "+ ReferenceNum);
		createLineItemTrans(this.apickli, ReferenceNum, callback);		
		
	});
	
	this.When(/^I use POST lineItems using lineItem Body (.*)$/, function (lnbody, callback) {		
		
		var ReferenceNum = this.apickli.scenarioVariables.Reference;
		console.log("RefNumber for POST lineItems : "+ ReferenceNum);
		createLineItemTransWithBody(this.apickli, ReferenceNum, lnbody, callback);		
		
	});
	
	this.When(/^I use POST lineItems using Invalid RefNumber and Valid lineItem Body (.*)$/, function (lnbody, callback) {			
		var invRefNum = "abcabcabc";
		console.log("Inv RefNumber for POST lineItems : "+ invRefNum);
		createLineItemTransWithInvRef(this.apickli, invRefNum, lnbody, callback);		
		
	});
	
	this.When(/^I use GET lineItems using Valid Reference Number$/, function (lnbody, callback) {		
		
		var ReferenceNum = this.apickli.scenarioVariables.Reference;
		console.log("RefNumber for POST lineItems : "+ ReferenceNum);
		createLineItemTransWithBody(this.apickli, ReferenceNum, lnbody, callback);		
		
	});
	
	this.When(/^I use GET lineItems with Valid Reference Number$/, function (callback) {
      	var ReferenceNum = this.apickli.scenarioVariables.Reference;
		console.log("Get LinRefNumber : "+ ReferenceNum);
		getChargesLineItem(this.apickli, ReferenceNum, callback);

	});
	
	this.When(/^I use GET lineItems with InValid Reference Number = (.*)$/, function (invRefNum, callback) {      
		console.log("Inv RefNumber for POST lineItems : "+ invRefNum);
		getChargesLineItem(this.apickli, invRefNum, callback);

	});
	
	this.When(/^I use GET lineItems with Blank Reference Number$/, function (callback) {
      	var BlankRefNum = " ";
		console.log("Inv RefNumber for POST lineItems : "+ BlankRefNum);
		getChargesLineItem(this.apickli, BlankRefNum, callback);

	});
	
	this.When(/^I use DELETE lineItems with Valid Reference Number$/, function (callback) {
      	var ReferenceNum = this.apickli.scenarioVariables.Reference;
		console.log("Delete LinRefNumber : "+ ReferenceNum);
		deleteChargesLineItem(this.apickli, ReferenceNum, callback);

	});
	
	this.When(/^I use DELETE lineItems with InValid Reference Number$/, function (callback) {
      	var invRefNum = "abcabcabc";
		console.log("Inv RefNumber for Delete lineItems : "+ invRefNum);
		deleteChargesLineItem(this.apickli, invRefNum, callback);

	});
	
};
