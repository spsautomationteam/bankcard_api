/* jslint node: true */
'use strict';

// Create a new singleton
var hmacTools = new (require("../../../hmacTools.js"))();
var async = require('async');

module.exports = function () {

    this.Given(/^I have valid credentials$/, function (callback) {
        // Already set by config and init.js
        callback();
    });
    
    this.Given(/^I have a valid request$/, function (callback) {
        
        var nonces = hmacTools.secureNonce(16);
        this.apickli.scenarioVariables.nonces = nonces;
        this.apickli.scenarioVariables.currentRequest = {
            amount: "28",
            billing: {},
            environment: "cert",
            merchantId: this.apickli.scenarioVariables.merchantId,
            merchantKey: this.apickli.scenarioVariables.merchantKey,
            nonce: nonces.salt,
            postbackUrl: "http://www.example.com",
            preAuth: false,
            requestId: "Invoice" + hmacTools.nonce(3)
        }
        callback();
    });
    
    this.Given(/^I set (.*) to (\d+)$/, function(target, value, callback){
        this.apickli.scenarioVariables.currentRequest[target] = value;
        callback();
    });
    
    this.When(/^I generate an authKey$/, function(callback){
        
        var authKey = hmacTools.encrypt(
            this.apickli.scenarioVariables.clientSecret,
            this.apickli.scenarioVariables.nonces.iv,
            this.apickli.scenarioVariables.nonces.salt,
            JSON.stringify(this.apickli.scenarioVariables.currentRequest)
        );
        this.apickli.scenarioVariables.currentRequest["authKey"] = authKey;
		//console.log(authKey);
        callback();
    })

    this.When(/^I POST to (.*)$/, function (pathSuffix, callback) {
        this.apickli.headers['clientId'] = this.apickli.scenarioVariables.clientId;
        this.apickli.headers['Content-Type'] = 'application/json';
        this.apickli.requestBody = JSON.stringify(this.apickli.scenarioVariables.currentRequest);
        this.apickli.post(pathSuffix, function (err, response) {
            callback(err);
        });
    });

};
