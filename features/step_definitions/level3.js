/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();

var createCharge = function( apickli, callback ) {
    var pathSuffix = "/charges?type=Sale";
    var url = apickli.domain + pathSuffix;
    var body = { ECommerce: { Amounts: { Total: 1.0 }, CardData: { Expiration: 1216, Number: 4111111111111111 } } }
    var body = JSON.stringify(body);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
    apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
    apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
    apickli.addRequestHeader('nonce', nonce);
    apickli.addRequestHeader('timestamp', timestamp);
    apickli.addRequestHeader('Authorization', hmac);

    apickli.post(pathSuffix, function (err, response) {
        apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'saleReference');
        callback();
    });
};

var addLineItem = function( apickli, saleReference, callback ) {
    var pathSuffix = "/charges/" + saleReference + "/lineitems";
    var url = apickli.domain + pathSuffix;
    var body = {visa:[{commodityCode:"12345",itemDescription:"My Item Description",productCode:"ProductCode",quantity:1,unitOfMeasure:"Units",unitCost:1,vatTaxAmount:0,vatTaxRate:0,discountAmount:0,lineItemTotal:1}]}
    var body = JSON.stringify(body);

    apickli.setRequestBody(body);
    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;

    apickli.post(pathSuffix, function (err, response) {
        callback();
    });
};

var getLineItems = function( apickli, saleReference, callback ) {
    var pathSuffix = "/charges/" + saleReference + "/lineitems";
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;
    apickli.setRequestBody("");

    apickli.get(pathSuffix, function (err, response) {
        callback();
    });
};

var deleteLineItems = function( apickli, saleReference, callback ) {
    var pathSuffix = "/charges/" + saleReference + "/lineitems";
    var url = apickli.domain + pathSuffix;

    var nonce = hmacTools.nonce(12);
    var timestamp = Date.now() / 1000 + '';

    var clientSecret = apickli.scenarioVariables.clientSecret;
    var hmac = hmacTools.hmac( clientSecret, "DELETE", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
    apickli.headers['nonce'] = nonce;
    apickli.headers['timestamp'] = timestamp;
    apickli.headers['Authorization'] = hmac;
    apickli.setRequestBody("");

    apickli.delete(pathSuffix, function (err, response) {
        callback();
    });
};

module.exports = function () { 
	this.Given(/^I create a charge$/, function (callback) {
        createCharge(this.apickli, callback);
	});

	this.When(/^I add a line item to that same charge$/, function (callback) {
        var saleReference = this.apickli.scenarioVariables.saleReference;
		addLineItem(this.apickli, saleReference, callback);
	});
    
    this.When(/^I get the line items on that charge$/, function (callback) {
        var saleReference = this.apickli.scenarioVariables.saleReference;
		getLineItems(this.apickli, saleReference, callback);
	});
    
    this.When(/^I delete the line items on that charge$/, function (callback) {
        var saleReference = this.apickli.scenarioVariables.saleReference;
		deleteLineItems(this.apickli, saleReference, callback);
	});
};