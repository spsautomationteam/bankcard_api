/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createSaleAuthForceTrans = function( apickli,TxType, callback ) {
		var pathSuffix = "/charges?type="+TxType;
		var url = apickli.domain + pathSuffix;
		
		var totAmount =Math.floor(Math.random() * 1000);
		
		var body = { "transactionId": "", "retail": { "amounts": { "tip": 4.24, "total": totAmount, "tax": 2.12, "shipping": 1.06 }, "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",  "state": "VA", "postalCode": "20190", "country": "US" }, "isRecurring": false } };
		
		var body = JSON.stringify(body);
		
		console.log("Body Total : "+ totAmount)	
		
	//	console.log("Body Amount "+body.total);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Transaction RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;		
						
        	callback();     
		});
};

var createTrans = function( apickli,TxType, Txbody, callback ) {
		var pathSuffix = "/charges?type="+TxType;
		//var pathSuffix = "/charges/reference/lineitem;
		var url = apickli.domain + pathSuffix;
		var body = Txbody;
		console.log(body);
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
		
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Transaction RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;		
						
        	callback();     
		});
};


var getCharge = function( apickli, Reference, callback ) {
		var pathSuffix = "/charges/" + Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var captureAuthorization = function( apickli, pathSuffix, Amount, callback ) {
	
	//	var pathSuffix = "/charges/" + AuthReference;
		var url = apickli.domain + pathSuffix;
		// var body = { amounts: { total: Amount, tax: 0, shipping: 0 } }
		var body = { "amounts": { "tip": 4.24, "total": Amount,  "tax": 2.12, "shipping": 1.06 } };
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp 
		= Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "PUT", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.put(pathSuffix, function (err, response) {
			if ( response ) {
				console.log(response.statusCode);
				console.log(err);
			}
        		callback();
		});
		
};

module.exports = function () { 
	this.Given(/^I have valid merchant credentials$/, function (callback) {
     //   this.apickli.storeValueInScenarioScope("clientId", "QAYEWMGah32YPYtNATKSEAfybEoNVRJL");
     //   this.apickli.storeValueInScenarioScope("clientSecret", "246Ly5PzOfIoE3uo");
     //   this.apickli.storeValueInScenarioScope("merchantId", "999999999997");
     //   this.apickli.storeValueInScenarioScope("merchantKey", "K3QD6YWyhfD");	
		
		this.apickli.storeValueInScenarioScope("clientId", this.apickli.scenarioVariables.clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", this.apickli.scenarioVariables.clientSecret);
        this.apickli.storeValueInScenarioScope("merchantId", this.apickli.scenarioVariables.merchantId);
        this.apickli.storeValueInScenarioScope("merchantKey", this.apickli.scenarioVariables.merchantKey);		
		
		// console.log( "Client ID:---------- " + this.apickli.scenarioVariables.clientId);
		// console.log( "Merchant ID:---------- " + this.apickli.scenarioVariables.merchantId);
	
		callback();
	});
	
	this.Given(/^I perform a (.*) Transaction$/, function (TxType, callback) {
		createSaleAuthForceTrans(this.apickli,TxType, callback);
		});

	
	this.When(/^I Capture the (.*) transaction with Total Amount (.*)$/, function (typeTrans, amount, callback) {		
		console.log("Capture Total : "+ amount);
		
		var authReference = this.apickli.scenarioVariables.Reference;
		console.log(typeTrans+" RefNumber: "+ authReference);
		var pathSuffix = "/charges/" + authReference;
		captureAuthorization(this.apickli, pathSuffix, amount, callback);		
		
	});
	
	this.When(/^Again try to Capture the Same transaction$/, function (callback) {	
	
		var amount = 10;
		var authReference = this.apickli.scenarioVariables.Reference;
		//console.log(" RefNumber: "+ authReference);
		var pathSuffix = "/charges/" + authReference;
		captureAuthorization(this.apickli, pathSuffix, amount, callback);		
		
	});
	
	this.Given(/^Again try to perform same (.*) transaction with Total Amount (.*)$/, function (TxType, totAmount, callback) {
		createSaleAuthForceTrans(this.apickli,TxType, totAmount, callback);
	});

	this.When(/^I get that same charge$/, function (callback) {
        var Reference = this.apickli.scenarioVariables.Reference;
		// console.log("REFERENCE: ", authorizationReference );
		getCharge(this.apickli, Reference, callback);
	});
	
	this.When(/^Try to Capture the Transaction with Invalid Reference Number and Total Amount (.*)$/, function (amount, callback) {		
		
		var invRefNum = "f34rtf44fsde";
		console.log("Invalid RefNumber: "+ invRefNum);
		
		var pathSuffix = "/charges/" + invRefNum;
		captureAuthorization(this.apickli, pathSuffix, amount, callback);
	});
	
	this.When(/^Try to Capture the Transaction with Blank Reference Number and Total Amount (.*)$/, function (amount, callback) {		
		
		var invRefNum = "";
		console.log("Blank RefNumber: "+ invRefNum);
		
		var pathSuffix = "/charges/" + invRefNum;
		captureAuthorization(this.apickli, pathSuffix, amount, callback);
	});
	
};
