var apickli = require('apickli');
var config = require('../../config/config.json');

var defaultBasePath = config.apiSandBox.basepath;
var defaultDomain = config.apiSandBox.domain;

console.log('bankcard api: [' + config.apiSandBox.domain + ', ' + config.apiSandBox.basepath + ']');

var getNewApickliInstance = function(basepath, domain) {
	basepath = basepath || defaultBasePath;
	domain = domain || defaultDomain;

	return new apickli.Apickli('https', domain + basepath);
};

exports.getNewApickliInstance = getNewApickliInstance;
