/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createSale = function( apickli, callback ) {
		var pathSuffix = "/charges?type=Sale";
		var url = apickli.domain + pathSuffix;
		
		var amount =Math.floor(Math.random() * 1000);		

		apickli.storeValueInScenarioScope("Total", amount);	
		
		var body = {     "transactionId": "",     "retail": {         "amounts": {             "tip": 0,             "total":amount,             "tax": 0,             "shipping": 0         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		var body = JSON.stringify(body);
		console.log("**********"+body);
		// console.log("\n before replace  body >>>*: "+body);
					
		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);
		//console.log("\n\n hmac :::::"+hmac)

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;
			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'OrderNumber');			
			console.log("Trans OrderNumber : "+ apickli.scenarioVariables.OrderNumber)	
			var OrderNumber = apickli.scenarioVariables.OrderNumber;
			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.code', 'TransCode');			
			console.log("Trans Code : "+ apickli.scenarioVariables.TransCode)	
			var OrderNumber = apickli.scenarioVariables.TransCode;
						
        	callback();     
		});
};


var batchSettle = function(apickli,callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = {"settlementType": "Bankcard"};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			//	apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
				
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'BatchReference');			
			console.log("Batch settle Reference : "+ apickli.scenarioVariables.BatchReference)	
			var BatchReference = apickli.scenarioVariables.BatchReference;
			
			}
        	callback();
		});
};


module.exports = function () { 

	this.Given(/^I create a sale transaction$/, function (callback) {
		createSale(this.apickli, callback);
	});
	
	this.When(/^I settle all current set of transaction$/, function (callback) {
       
		batchSettle(this.apickli,callback);
	});

	};
