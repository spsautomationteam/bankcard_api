/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var createPostBatchCurrentTx = function( apickli, callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = {"settlementType": "Bankcard"};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Credit Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;		
			
        	callback();
		});
};

var createPostBatchCurrentBody = function( apickli, TxBody, callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = { "settlementType": "Bankcard",     "count": 10,     "net": 10,     "terminalNumber": "" };
	//	var body = {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":36.45},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"121"}}};
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Credit Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;		
			
        	callback();
		});
};

var getBatchList = function(apickli,callback ) {
		var pathSuffix = "/batches";
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';
		
		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				
			}
        	callback();
		});
});
};

var getBatchCurrentList = function(apickli,callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';
		
		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				
			}
        	callback();
		});
});
};

var getBatchCurrentSummaryList = function(apickli,callback ) {
		var pathSuffix = "/batches/current/summary";
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';
		
		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
		apickli.headers['merchantId'] = apickli.scenarioVariables.merchantId;
		apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
        apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.headers['Authorization'] = hmac;

		apickli.get(pathSuffix, function (err, response) {
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
				
			}
        	callback();
		});
});
};

var verifyReference = function( apickli, callback ) {
		var Reference = apickli.scenarioVariables.Reference;		
		var assertion = apickli.assertResponseBodyContainsExpression(Reference); 
        if (assertion.success) {
            callback();
        } else {
            callback(prettyPrintJson(assertion));
        }  
				
		
};

var getBatch = function( apickli, Reference, callback ) {
		var pathSuffix = "/batches/" + Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getBatchRefPageNum = function( apickli, BatchReference, pageNumber, callback ) {
		var pathSuffix = "/batches/"+BatchReference+"?"+"pageNumber="+pageNumber;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getBatchRefPageSize = function( apickli, BatchReference, pageSize, callback ) {
		var pathSuffix = "/batches/"+BatchReference+"?"+"pageSize="+pageSize;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getCurrentBatch = function( apickli, callback ) {
		var pathSuffix = "/batches/current";
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getBatchTransaction = function( apickli, Reference, callback ) {
	
		var pathSuffix = "/batches/"+Reference;
		var url = apickli.domain + pathSuffix;
		
		console.log( "pathSuffix : "+ pathSuffix);

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';
		
		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


module.exports = function () { 

this.When(/^I perform Valid Post Batches Current transaction$/, function (callback) {
		createPostBatchCurrentTx(this.apickli, callback);
	});
	
	this.When(/^I perform Valid Post Batches Current transaction with Body $/, function (TxBody, callback) {
		createPostBatchCurrentTx(this.apickli, TxBody, callback);
	});
	
	this.When(/^I get List of Existing Batche Transactions$/, function (callback) {       
		getBatchList(this.apickli,callback);
	});
	
	this.When(/^I get List of Existing Current Batche Transactions$/, function (callback) {       
		getBatchCurrentList(this.apickli,callback);
	});
	
	this.When(/^I get List of Existing Current Batche Summary Transactions$/, function (callback) {       
		getBatchCurrentSummaryList(this.apickli,callback);
	});
	
	this.When(/^I get that same existing batch detail$/, function (callback) {
       
		var BatchReference = this.apickli.scenarioVariables.BatchReference;
		getBatch(this.apickli,BatchReference,callback);
	});
	
	this.When(/I get List of Transactions for an Existing Batch$/, function (callback) {
       
		var BatchReference = this.apickli.scenarioVariables.BatchReference;
		getBatchTransaction(this.apickli,BatchReference,callback);
	});
	
	this.When(/^I tried to GET Batches with Valid Reference and PageNumber = (.*)$/, function (pageNumber, callback) {
        var BatchReference = this.apickli.scenarioVariables.BatchReference;
		console.log("RefNum for GET Batches : ", BatchReference);
		getBatchRefPageNum(this.apickli, BatchReference, pageNumber, callback);
	});
	
	
	this.When(/^I tried to GET Batches with Valid Reference and PageSize = (.*)$/, function (pageSize, callback) {
        var BatchReference = this.apickli.scenarioVariables.BatchReference;
		console.log("RefNum for GET Batches : ", BatchReference);
		getBatchRefPageSize(this.apickli, BatchReference, pageSize, callback);
	});	
	
	
	this.When(/^I GETS the Batche transaction with InValid Reference Number = (.*)$/, function (invRefNum, callback) {     
		console.log("InValid RefNum for GET Batches Reference Details: "+invRefNum);	
		getBatchTransaction(this.apickli, invRefNum, callback);
	});
	
	
	this.Then(/^response body path should contain above settled batch reference$/, function (callback) {
		//var Reference = this.apickli.scenarioVariables.Reference;
		//console.log(Reference);
		verifyReference(this.apickli, callback);		
		
	});

	};
