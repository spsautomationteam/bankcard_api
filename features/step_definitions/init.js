/* jshint node:true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');

var invalidClientId = config.apiSandBox.invalidClientId;
var clientId = config.apiSandBox.clientId;
var clientSecret = config.apiSandBox.clientSecret;
var merchantId = config.apiSandBox.merchantId;
var merchantKey = config.apiSandBox.merchantKey;

module.exports = function() {
    // cleanup before every scenario
    this.Before(function(scenario, callback) {
        this.apickli = factory.getNewApickliInstance();
        this.apickli.storeValueInScenarioScope("invalidClientId", invalidClientId);
        this.apickli.storeValueInScenarioScope("clientId", clientId);
        this.apickli.storeValueInScenarioScope("clientSecret", clientSecret);
		this.apickli.storeValueInScenarioScope("merchantId", merchantId);
        this.apickli.storeValueInScenarioScope("merchantKey", merchantKey);
		
		console.log( "Client ID:---------- " + clientId);
		console.log( "Merchant ID:---------- " + merchantId);
		
        callback();
    });
};

