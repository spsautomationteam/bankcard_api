/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var hmacTools = new (require("../../../hmacTools.js"))();
var prettyJson = require('prettyjson');
var Contenttype = "application/json";

var stepContext = {};

var createCreditTx = function( apickli, callback ) {
		var pathSuffix = "/credits";
		var url = apickli.domain + pathSuffix;
		
		var totAmount =Math.floor(Math.random() * 1000);
		
	//	var body = { ECommerce: { Amounts: { Total: 15.0 }, CardData: { Expiration: 1216, Number: 4111111111111111 } } };
		var body = {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":totAmount},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"121"}}};
	//	var body = {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "vinayak@foo.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } };
		var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Credit Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;

			apickli.storeValueOfResponseBodyPathInScenarioScope('$.orderNumber', 'OrderNumber');			
			console.log("Credit Trans OrderNumber : "+ apickli.scenarioVariables.OrderNumber)	
			var OrderNumber = apickli.scenarioVariables.OrderNumber;		
				
			
        	callback();
		});
};

var createTxWithBody = function( apickli, TxBody, callback ) {
	
		var pathSuffix = "/credits";
		var url = apickli.domain + pathSuffix;
		var body = TxBody;
		//var body = {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":36.45},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"121"}}};
		// var body = JSON.stringify(body);
		

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';		

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.addRequestHeader('Content-Type', Contenttype);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);

		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
			console.log("Credit Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;		
			
        	callback();
		});
};

var createPostCreditRefTransWithBody = function(apickli, TxReference, Txbody, callback ) {	

	var localApickli = factory.getNewApickliInstance(apickli.scenarioVariables.basepath);
	
	var pathSuffix = "/credits/"+TxReference;
	var url = apickli.domain + pathSuffix;
	console.log( "URL ***** = "+url );
	//var body = {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
     var body = Txbody;
     // var reqString =body;
	 console.log( "BODY ***** = "+body );	 
        
	//var body = reqString; 
	 //JSON.stringify(reqString);
          //var body = JSON.stringify(body);

      localApickli.setRequestBody(body);
       var nonce = hmacTools.nonce(12);
       var timestamp = Date.now() / 1000 + '';

        var clientSecret = apickli.scenarioVariables.clientSecret;
        var hmac =  hmacTools.hmac( clientSecret, "POST", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);// "POST", url, body, '', nonce, timestamp);
        localApickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        localApickli.addRequestHeader('nonce', nonce);
        localApickli.addRequestHeader('timestamp', timestamp);
        localApickli.addRequestHeader('Authorization', hmac);
        localApickli.addRequestHeader('Content-Type', 'application/json');
        localApickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
		localApickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);

        localApickli.post(pathSuffix, function (err, response) {		
		if ( response ) {
				console.log( response.body);
			}
					
			localApickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
		//	console.log("Post Credit Trans RefNumber : "+ apickli.scenarioVariables.Reference)	
			var Reference = apickli.scenarioVariables.Reference;
                            
        callback();//(response, apickli);
		
  });
};

var getCreditLists = function( apickli, callback ) {
		var pathSuffix = "/credits";
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['clientId'] = apickli.scenarioVariables.clientId;
        apickli.headers['merchantId'] =apickli.scenarioVariables.merchantId;
        apickli.headers['merchantKey'] = apickli.scenarioVariables.merchantKey;
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};

var getCredit = function( apickli, Reference, callback ) {
		var pathSuffix = "/credits/"+ Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;

		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


module.exports = function () { 

	this.When(/^I perform Valid Credit transaction$/, function (callback) {
		createCreditTx(this.apickli, callback);
	});
	
	this.When(/^I try to perform Same Credit transaction again$/, function (callback) {
		createCreditTx(this.apickli, callback);
	});
	
	this.When(/^I perform Valid Credit transaction with Body (.*)$/, function (TxBody, callback) {
		createTxWithBody(this.apickli, TxBody, callback);
	});
	
	this.When(/^I get the list of credit transactions$/, function ( callback) {
      	//this.apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
		getCreditLists(this.apickli, callback);

	});
	
	this.When(/^I get that Same credit Transaction$/, function (callback) {
        var Reference = this.apickli.scenarioVariables.Reference;
		getCredit(this.apickli, Reference, callback);
	});
	
	this.When(/^I use POST Credits Reference using Body (.*)$/, function (Txbody, callback) {
       var ReferenceNum = this.apickli.scenarioVariables.Reference;
		console.log("RefNumber for POST Credit Reference : "+ ReferenceNum);
		createPostCreditRefTransWithBody(this.apickli, ReferenceNum, Txbody, callback);
	});
	
	this.When(/^I use POST Credit Reference with InValid Reference and Valid lineItem Body (.*)$/, function (Txbody, callback) {	
		var invRefNum = "abcabcabc";
		console.log("InValid RefNum for Post Credit : "+invRefNum);			
		createPostCreditRefTransWithBody(this.apickli, invRefNum, Txbody, callback);
	});
	
};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
