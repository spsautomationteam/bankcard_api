/* jslint node: true */
'use strict';

var factory = require('./factory.js');
var config = require('../../config/config.json');var hmacTools = new (require("../../../hmacTools.js"))();
var Contenttype = "application/json";

var getCreditTransactionDetails = function( apickli, Reference, callback ) {
		var pathSuffix = "/credits/" + Reference;
		var url = apickli.domain + pathSuffix;

        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "GET", url, "", apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('merchantId', apickli.scenarioVariables.merchantId);
        apickli.addRequestHeader('merchantKey', apickli.scenarioVariables.merchantKey);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		
		apickli.get(pathSuffix, function (err, response) {
			if ( response ) {
				console.log( response.body);
			}
        	callback();
		});
};


var DeleteCreditTx = function( apickli, Reference, callback ) {
		var pathSuffix = "/credits/" + Reference;
		var url = apickli.domain + pathSuffix;
		var body = '';
		//var body = JSON.stringify(body);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac( clientSecret, "DELETE", url, body, apickli.scenarioVariables.merchantId, nonce, timestamp);
		apickli.headers['nonce'] = nonce;
		apickli.headers['timestamp'] = timestamp;
		apickli.headers['Authorization'] = hmac;
		apickli.headers['Content-Type'] = Contenttype;
		apickli.delete(pathSuffix, callback);
		
};
module.exports = function () { 
	
	this.When(/^I Delete the Credit transaction with Valid Reference Number$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		console.log("RefNum for Delete Credit : "+Reference);		
		DeleteCreditTx(this.apickli, Reference, callback);
	});
	
	this.When(/^I again try to Delete the Same Credit transaction$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		console.log("RefNum for 2nd Time Delete Credit : "+Reference);		
		DeleteCreditTx(this.apickli, Reference, callback);
	});
	
	this.When(/^I Delete the Credit transaction with InValid Reference Number$/, function (callback) {		
		var invRefNum = "abcabcabc";
		console.log("InValid RefNum for Delete Credit : "+invRefNum);		
		DeleteCreditTx(this.apickli, invRefNum, callback);
	});
	
	this.When(/^I try to Delete Post Charges transaction at Credit Delete Screen$/, function (callback) {
		var Reference = this.apickli.scenarioVariables.Reference;
		console.log("Post Charges RefNum for Delete Credit : "+Reference);		
		DeleteCreditTx(this.apickli, Reference, callback);
	});
	
	this.When(/^I GETS the Credit transaction with Valid Reference Number$/, function (callback) {
        var Reference = this.apickli.scenarioVariables.Reference;
		console.log("RefNum for GET Credit : ", Reference);
		getCreditTransactionDetails(this.apickli, Reference, callback);
	});
	
	this.When(/^I GETS the Credit transaction with InValid Reference Number = (.*)$/, function (invRefNum, callback) {
		console.log("InValid RefNum for GET Credit Details: "+invRefNum);	
		getCreditTransactionDetails(this.apickli, invRefNum, callback);
	});

	};
