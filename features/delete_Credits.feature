@Delete_Credits
Feature: Delete Credits
	As an API consumer
	I want to Void an Existing Charge Transaction

	@delete_Credits_byCreditReference
	Scenario: Perform a Credit transaction and Delete the same transaction with Valid Reference Number
		Given I have valid merchant credentials
		When I perform Valid Credit transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}	
		Then response code should be 201   
		And I Delete the Credit transaction with Valid Reference Number
		Then response code should be 200		
		
		
	#  ********************** Negative Scenarios ***********************
		
	@delete_Credits_byDeleteTransactions_Twice
	Scenario: Try to delete already deleted credit transaction
		Given I have valid merchant credentials
		When I perform Valid Credit transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}	
		Then response code should be 201   
		And I Delete the Credit transaction with Valid Reference Number		
		Then response code should be 200
		And I again try to Delete the Same Credit transaction		
		Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.		
		
#	@Delete_Credits_With_PostCharges_ReferenceNumber
#	Scenario: Try to delete Post Charges transaction at Delete Credits screen
#		Given I have valid merchant credentials
#		And I perform a Sale transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
#		Then response code should be 201   
#		And I try to Delete Post Charges transaction at Credit Delete Screen		
#		Then response code should be 200
		
			
#	@Delete_Credits_byBlankReferenceNumber
#    Scenario: Verify the API Delete_Credits with Blank Reference Number
#        Given I set clientId header to `clientId`
#        And I set apikey header to `clientId`
#        And I set merchantId header to `merchantId`
#        And I set merchantKey header to `merchantKey`
#	     And I set authorization header to `authorization`  
#	     When Verify Delete Credits with Blank Reference Number
#	     When I use HMAC and GET /credits/fsdfdffsdf
#        Then response code should be 404
#		 And response body path $.message should be Internal Server Error
#		 And response body path $.detail should be Please contact support for assistance.
		
	@Delete_Credits_byInvalidReferenceNumber
    Scenario: Verify the API Delete_Credits with Invalid Reference Number
        Given I have valid merchant credentials
        And I Delete the Credit transaction with InValid Reference Number			
		Then response code should be 401
		And response body path $.code should be 100005
		And response body path $.message should be Missing or invalid Application Identifier
		And response body path $.detail should be A valid Application ID is required in header parameter clientId.
		
	
