@credits_POST
Feature: Post Credits
	As an API consumer
	I want to create charge requests
	So that I know they can be processed	
	
#	@post_Credits-bySaleAuthForce
#	Scenario Outline: Perform a transaction and then delete that respective transaction 
#		Given I have valid merchant credentials
#		And I perform a <TypeOfTransaction> transaction
#		Then response code should be 201
#		And response header Content-Type should be application/json
#       And response body path $.status should be Approved	
#		And response body path $.message should be APPROVED	
#		And I get that Same credit Transaction
#       Then response code should be 200
#		Examples:
#		|	TypeOfTransaction	|
#		|	Sale				|
#		|	Authorization		|
#		|	force				|	
		
	@post_Credits-byCreditReference
	Scenario: Perform a transaction and then delete that respective transaction 
		Given I have valid merchant credentials
		And I perform Valid Credit transaction
		Then response code should be 201
		And response header Content-Type should be application/json
        And response body path $.status should be Approved	
		And response body path $.message should be APPROVED	
		And I get that Same credit Transaction
        Then response code should be 200
		
		
	@post-Credits-ValidTransaction	
	Scenario: Post a credit and make it a recurring action
		Given I have valid merchant credentials
		And I perform Valid Credit transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		Then response code should be 201
		And response header Content-Type should be application/json
        And response body path $.status should be Approved	
		And response body path $.message should be APPROVED	
		And I get that Same credit Transaction
        Then response code should be 200

	@post-Credits-WithParameterizeData
	Scenario Outline: API Post Credits With Parameterize Data for all Cards
		Given I have valid merchant credentials
		When I perform Valid Credit transaction with Body {"transactionId": "",     "<Type>": {         "amounts": {             "tip": 4.24,             "total": <Amount>,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "<OrderNumber>",         "cardData": {             "number": "<CardNumber>",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "<Email>",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "<CustomerName>",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": <IsRecurring>      } }
		Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.status should be Approved
		And response body path $.message should be APPROVED
		
		Examples:
		|Type       |CardNumber 	   |Amount  |OrderNumber | CustomerName  | Email 			|IsRecurring |
		|retail     |4111111111111111  | 22.13  |1234567     | San Kumar     |vinayak@foo.com   |false |
		|eCommerce  |5499740000000057  | 26.13  |1234568 	 | Midhun Kumar  |midhun@test.com   |true  |
		|healthcare |6011000993026909  | 27.13  |1234569 	 | Sateesh Kumar |sateesh@gmail.com |false |
		|eCommerce  |371449635392376   | 28.13  |1234560 	 | Bharath Kumar |bharath@yahoo.com |true  |	
		
		
		# **************  Negative Scenarios  *************************
		
	
	@post-Credits_byInvalidAmount
    Scenario: Post a credit for invalid Amount
       Given I have valid merchant credentials
		And I set body to {"transactionId": "tid-1234", "ECommerce": { "Amounts": { "Total": -48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits?type=credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be Invalid Total Amount	
		
	@post-Credits_byInvalidCardNumber
    Scenario: Post a credit for invalid CardNumber
       Given I have valid merchant credentials
		And I set body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"1111111111111111","cvv":"1234"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
		When I use HMAC and POST to /credits?type=credit
    #    Then response code should be 401
	#	And response body path $.code should be 100008
	#	And response body path $.message should be Invalid message request content
	#	And response body path $.detail should be You are using certification environment which is restricted to process transaction using test Credit cards only.
		
	    Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be InvalidRequestData : INVALID C_CARDNUMBER
	
	
	@post-Credits_byInvalidCVV
    Scenario: Post a credit for invalid CVV
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv": "@#$%*@#$*" } } }		
		When I use HMAC and POST to /credits?type=credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The field CVV must be a string or array type with a maximum length of '4'.
		
	@post-Credits_byCVVMoreThan4Digits
    Scenario: Post a credit for CVV value More Than 4 Digits 
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "1216", "Number": "4111111111111111", "cvv": "12345" } } }		
		When I use HMAC and POST to /credits?type=credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The field CVV must be a string or array type with a maximum length of '4'.
     
	@post-Credits_byInvalidEmailId
    Scenario: Post a credit for invalid EmailId
        Given I have valid merchant credentials
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "sankumar.com",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The Email field is not a valid e-mail address.
		
	@post-Credits_byBlankEmailId
    Scenario: Post a credit for Blank EmailId
        Given I have valid merchant credentials
		And I set body to {     "transactionId": "",     "retail": {         "amounts": {             "tip": 4.24,             "total": 42.42,             "tax": 2.12,             "shipping": 1.06         },         "authorizationCode": "464646",         "orderNumber": "7687677",         "cardData": {             "number": "4111111111111111",             "expiration": "1220",             "cvv": "1234"         },         "customer": {             "email": "",             "telephone": "7033334444",             "fax": "7033334444"         },         "billing": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "shipping": {             "name": "Vinayak Bhat",             "address": "123Main St.",             "city": "Reston",             "state": "VA",             "postalCode": "20190",             "country": "US"         },         "isRecurring": false      } }
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The Email field is not a valid e-mail address.
		
	@post-Credits_byInvalidExpDate
    Scenario: Post a credit for invalid Expiry Date
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20-12", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits-Credit_byExpDateLessThan4Digits
    Scenario: Post a credit by Expire Date Less Than 4 Digits
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits-Credit_byExpDateGreaterThan4Digits
    Scenario: Post a credit by Expire Date Greater Than 4 Digits
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "20255", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The field Expiration must be a string with a minimum length of 4 and a maximum length of 4.
		
	@post-Credits_byBlankExpireDate
    Scenario: Post a credit for Blank Expiry Date
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be The Expiration field is required.
		
	@post-Credits_ExpireDateWithNonDigits
    Scenario: Post a credit forExpiry Date with Non Digits
        Given I have valid merchant credentials
		And I set body to { "transactionId": "tid-1234","ECommerce": { "Amounts": { "Total": 48.0 },"authorizationCode": "123456","CardData": { "Expiration": "abcd", "Number": "4111111111111111" } } }		
		When I use HMAC and POST to /credits?type=Credit
        Then response code should be 400
		And response body path $.code should be 400000
		And response body path $.message should be There was a problem with the request. Please see 'detail' for more.
		And response body path $.detail should be Expiration must be a number
	
