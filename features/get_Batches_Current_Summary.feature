@get_Batches_Current_Summary
Feature: Get_Batches_Current_Summary
	As an API consumer
	I want to query batches requests
	So that I know they have been processed
		
	@get_Batches_Current_Summary_AllSummaryTransactions
    Scenario: Get all 'Get Batches Current Summary' transactions
		Given I have valid merchant credentials
    #   When I use HMAC and GET /batches/current/summary
		When I get List of Existing Current Batche Summary Transactions
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain "paymentType"
		And response body should contain "authCount"
		And response body should contain "authTotal"
		And response body should contain "saleCount"
		And response body should contain "saleTotal"
		And response body should contain "creditCount"
		And response body should contain "creditTotal"
		And response body should contain "totalCount"
		And response body should contain "totalVolume"	
		
		
		############# Negative Scenarios  #####################		
		
		
	@get_Batches_Current_Summary_byinValidReference
	Scenario: Try to get transaction summery with invalid Reference 
        Given I have valid merchant credentials
		When I use HMAC and GET /batches/Test1234/summary
		Then response code should be 200
		And response body should contain "authCount":0
		And response body should contain "authTotal":0
		And response body should contain "saleCount":0
		And response body should contain "saleTotal":0
		And response body should contain "creditCount":0
		And response body should contain "creditTotal":0
		And response body should contain "totalCount":0
		And response body should contain "totalVolume":0
		
		
