@Get_Charges
Feature: Get_Charges
	As an API consumer
	I want to query charge requests
	So that I know they have been processed

	@get-All-Charges
    Scenario Outline: query all charges
        Given I have valid merchant credentials
		When I perform a <TypeOfTransaction> transaction
	#	When I use HMAC and GET a list of existing <TypeOfTransaction> transaction
		When I get a list of existing <TypeOfTransaction> transaction
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain above performed transaction reference
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference":
		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	force				|

	@get-Charges_byTransactionType
    Scenario Outline: query a specific charge
		Given I have valid merchant credentials
		When I perform a <TypeOfTransaction> transaction
	#	When I use HMAC and GET that same charge
		And I get that same charge
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body path should contain above performed transaction reference
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference":
   
		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	force				|
		
	@get-Charges_byTransType
    Scenario Outline: Verify the API Get charges with TransactionCode
        Given I have valid merchant credentials
        When I use HMAC and GET /charges?type=<TransactionType>
        Then response code should be 200
		And response header Content-Type should be application/json
        And response body should contain "type":"<TransactionType>"
	#	And response body path $.items.[transactionCode] should be <TransactionType>  
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
			
		Examples:
		|TransactionType  |
		|Sale			  |
		|Authorization    |
		|Force 			  |
	# 	|Void 			  |
		|Credit 		  |
	#	|CreditByReference|
		|Capture		  |	
	
		
	@get-Charges_byStartDate
    Scenario: Verify the API Get charges with StartDate
        Given I have valid merchant credentials
        When I use HMAC and GET /charges?startDate=2017-05-17
        Then response code should be 200
        And response body should contain "startDate":"2017-05-17" 
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
	@get-Charges_byEndDate
    Scenario: Verify the API Get charges with EndDate
       Given I have valid merchant credentials
        When I use HMAC and GET /charges?endDate=2017-05-22
        Then response code should be 200
        And response body should contain "endDate":"2017-05-22"
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
	@get-Charges_byPageSize
    Scenario: Verify the API Get charges with PageSize
        Given I have valid merchant credentials
        When I use HMAC and GET /charges?PageSize=1
        Then response code should be 200
        And response body should contain "pageSize":1
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"		
			
	@get-Charges_byPageNumber
    Scenario: Verify the API Get charges with PageNumber
        Given I have valid merchant credentials
        When I use HMAC and GET /charges?pageNumber=2
        Then response code should be 200
        And response body should contain "pageNumber":2	
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"		
		
	@get-Charges_byIsPurchaseCard
    Scenario Outline: Verify the API Get charges with IsPurchaseCard
       Given I have valid merchant credentials
        When I use HMAC and GET /charges?IsPurchaseCard=<IsPurchaseCard>
        Then response code should be 200
        And response body should contain "isPurchaseCard":<IsPurchaseCard>
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
		Examples:
		|IsPurchaseCard    |
	#	|true    		   |
		|false             |
		
	@get-Charges_byGatewayID
    Scenario: Verify the API Get charges with GatewayID
        Given I have valid merchant credentials
        When I use HMAC and GET /charges?GatewayId=`merchantId`
        Then response code should be 200
        And response body should contain "gatewayId":"`merchantId`"
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
	@get-Charges_byReferenceNumber
    Scenario: Verify the API Get charges with Bankcard ReferenceNumber
       Given I have valid merchant credentials		
	    And I perform a Sale transaction		
		When I get that same charge 			
        Then response code should be 200     
	    And response body should contain "reference":`reference`
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"			
		
	@get-Charges_byOrderNumber
    Scenario: Verify the API Get charges with Bankcard OrderNumber
        Given I have valid merchant credentials
		And I perform a Sale transaction		
		When I get that same charge 
        Then response code should be 200
		And response body should contain "orderNumber":`orderNumber`
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
	
	@get-Charges_byTotalAmount
    Scenario: Verify the API Get charges with TotalAmount
       Given I have valid merchant credentials
		And I perform a Sale transaction		
		When I get that same charge 		
        Then response code should be 200
		And response body should contain "total":`total`
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
	@get-Charges_byStatus
    Scenario Outline: Verify the API Get charges with Status
       Given I have valid merchant credentials
        When I use HMAC and GET /charges?Status=<Status>
        Then response code should be 200        
		And response body should contain "status":"<Status>"
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
		Examples:
		|Status  |
		|Declined|		
		|Batch   |
		|Settled |
	#	|Expired |
		
  #	commneted below scenario because of no charges done through Mobile 
   @get-Charges_bySource
   Scenario Outline: Verify the API Get charges with Source
  	Given I have valid merchant credentials
     When I use HMAC and GET /charges?Source=<Source>
     Then response code should be 200
     And response body should contain "applicationName":"<Value>"	
   
  Examples:
	|Source    | Value | 
  #	|Mobile    | SPS Mobile |
  	|Recurring | SPS Recurring |
		
	@get-Charges_byAccountNumber
    Scenario: Verify the API Get charges with AccountNumber
       Given I have valid merchant credentials
		And I perform a Sale transaction		
		When I get that same charge 
     #  When I use HMAC and GET /charges?AccountNumber=`accountNumber`
        Then response code should be 200
        And response body should contain "number":`number`		
		And response header Content-Type should be application/json
		And response body should contain "service":"Bankcard"	
		And response body should contain "reference"
		
		
		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++
		
	 @get-Charges_byinValidReference
     Scenario: Verify the API Get charges with inValidReference
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?Reference=fksjldfdsljfs
        Then response code should be 200
        And response body should not contain "reference":"fksjldfdsljfs"
		And response body should not contain "service":"Bankcard"	
		And response body should not contain "reference"
		
	@get-Charges_byFutureDate_AsStartDate
     Scenario: Verify the API Get charges with Future StartDate
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?StartDate=2030-12-12
        Then response code should be 200
        And response body should contain "startDate":"2030-12-12" 	
		And response body should not contain "reference"
		And response body should not contain "service"
		
	@get-Charges_byPastDate_AsEndDate
     Scenario: Verify the API Get Charges with Past EndDate
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?EndDate=2012-12-12
        Then response code should be 200
        And response body should contain "endDate":"2012-12-12"
		And response body should not contain "reference"
		And response body should not contain "service"
		
	@get-Charges_byinValidGatewayID
     Scenario: Verify the API Get charges with inValidGatewayId
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?GatewayID=FSFFSDFDSDFSF
        Then response code should be 200
        And response body should not contain "gatewayId":"FSFFSDFDSDFSF"		
		
	@get-Charges_byinValidOrderNumber
     Scenario: Verify the API Get charges with inValidOrderNumber
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?orderNumber=FSFFSDFDSDFSF
        Then response code should be 200
		And response body should not contain "orderNumber":"`FSFFSDFDSDFSF`"
		And response body should not contain "reference"
		And response body should not contain "service"
	
	@get-Charges_byinValidApprovalCode
     Scenario: Verify the API Get charges with inValidApprovalCode
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?approvalCode=FSFFSDFDSDFSF
        Then response code should be 200
        And response body should not contain "responseCode":"`FSFFSDFDSDFSF`"
		And response body should not contain "reference"
		And response body should not contain "service"
	
	@get-Charges_byinValidTransactionCode
     Scenario: Verify the API Get charges with inValidTransactionCode
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?TransactionCode=FSFFSDFDSDFSF
        Then response code should be 200
        And response body should not contain "transactionCode":"FSFFSDFDSDFSF"		
	
	@get-Charges_byinValidStatus
     Scenario: Verify the API Get charges with inValidStatus
       Given I have valid merchant credentials
		When I use HMAC and GET /charges?Status=fkjsdklfdsjsfdf
        Then response code should be 200
        And response body should not contain "status":"fkjsdklfdsjsfdf"
				
	@get-Charges_byinValidSource
     Scenario: Verify the API Get charges with inValidSource
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?Source=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"		
	
	@get-Charges_byinValidIsPurchaseCard
     Scenario: Verify the API Get charges with inValidPurchaseCard
        Given I have valid merchant credentials
		When I use HMAC and GET /charges?IsPurchaseCard=FSDFDSFDSFSFSSF
        Then response code should be 200
        And response body should not contain "source":"FSDFSDSFFSDFS"	
				
		
	
	