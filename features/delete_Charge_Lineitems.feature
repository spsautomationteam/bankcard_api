@Delete_Charges_LineItems
Feature: Post Charges
	As an API consumer
	I want to create charge requests
	So that I know they can be processed		
		
	@delete_Charges_Lineitems_Details_byValidRefNumber_Sale
	Scenario: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I have valid merchant credentials
		And I perform a Sale transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
		When I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
        And I use DELETE lineItems with Valid Reference Number
		Then response code should be 204  
#		And response body should contain No Content  		
		
	@delete_Charges_Lineitems_Details_byValidRefNumber_Force
	Scenario: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I have valid merchant credentials
		And I perform a Force transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
		When I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
        And I use DELETE lineItems with Valid Reference Number
		Then response code should be 204   		
		
	@delete_Charges_Lineitems_Details_byValidRefNumber_Authorization
	Scenario: API Get Charge Lineitems with Valid Reference Number and Lineitems Body
		Given I have valid merchant credentials
		And I perform a Authorization transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
		When I use POST lineItems using lineItem Body {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
        And I use DELETE lineItems with Valid Reference Number
		Then response code should be 204   		
		
		# **************  Negative Scenarios  *************************
		
	@delete_Charges_LineitemsDetails_byWithOut_PostLineItem_Sale
	Scenario: API Get Charge Lineitems with Valid Reference Number and WithOut PostLineItem
		Given I have valid merchant credentials
		And I perform a Sale transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
        And I use DELETE lineItems with Valid Reference Number
		Then response code should be 204 

	@delete_Charges_LineitemsDetails_byWithOut_PostLineItem_Force
	Scenario: API Get Charge Lineitems with Valid Reference Number and WithOut PostLineItem
		Given I have valid merchant credentials
		And I perform a Force transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
        And I use DELETE lineItems with Valid Reference Number
		Then response code should be 204  

	@delete_Charges_LineitemsDetails_byWithOut_PostLineItem_Authorization
	Scenario: API Get Charge Lineitems with Valid Reference Number and WithOut PostLineItem
		Given I have valid merchant credentials
		And I perform a Authorization transaction with Body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
        And I use DELETE lineItems with Valid Reference Number
		Then response code should be 204    		

	@delete_Charges_Lineitems_Details_byInValidRefNumber
	Scenario: API Get Charge Lineitems with InValid Reference Number and Lineitems Body
		Given I have valid merchant credentials
        And I use DELETE lineItems with InValid Reference Number 
		Then response code should be 401
		And response body path $.code should be 100005
		And response body path $.message should be Missing or invalid Application Identifier
		And response body path $.detail should be A valid Application ID is required in header parameter clientId.  		

#	@delete_Charges_Lineitems_Details_byBlankRefNumber
#	Scenario: API Get Charge Lineitems with Blank Reference Number
#		Given I have valid merchant credentials
#       And I use GET lineItems with Blank Reference Number 
#		Then response code should be 401
#		And response body path $.code should be 000000
#		And response body path $.message should be Internal Server Error
#		And response body path $.detail should be Please contact support for assistance. 

