@Get_Batches_Totals
Feature: Get_Batches_Totals
	As an API consumer
	I want to query batches requests
	So that I know they have been processed

#	@get-Batches-Totals_AllBatchTransactions
#    Scenario: query all batches
#       Given I have valid merchant credentials
#		When I perform Valid Post Batches Current transaction
#		And I set body to { "settlementType": "Bankcard",     "count": 10,     "net": 10,     "terminalNumber": "" }
#		When I use HMAC and POST to /batches/totals
#		And I get List of Existing Batche Transactions
#       Then response code should be 200
#		And response body should contain "count"
#		And response body should contain "net"
#		And response body should contain "volume"	
		
		
	@get-Batches-Totals_byStartDate
    Scenario: Verify the API 'Get Batches Totals' with StartDate
        Given I have valid merchant credentials
        When I use HMAC and GET /batches/totals?startDate=2017-06-13
        Then response code should be 200
		And response body should contain "count"
		And response body should contain "net"
		And response body should contain "volume"	
		
	@get-Batches-Totals_byEndDate
    Scenario: Verify the API 'Get Batches Totals' with EndDate
        Given I have valid merchant credentials
        When I use HMAC and GET /batches/totals?endDate=2017-07-10
        Then response code should be 200
		And response body should contain "count"
		And response body should contain "net"
		And response body should contain "volume"	
		
		
		# +++++++++++++++++    Negative Scinarios   ++++++++++++++++++++++++++	
	 
		
	@get-Batches-Totals_StartDate-AsFutureDate
     Scenario: Verify the API 'Get Batches Totals' with Future StartDate
        Given I have valid merchant credentials
		When I use HMAC and GET /batches/totals?StartDate=2030-12-12
        Then response code should be 200
		And response body should contain "count":0
		And response body should contain "net":0
		And response body should contain "volume":0
		
	@get-Batches-Totals_EndDate-AsPastDate
     Scenario: Verify the API 'Get Batches Totals' with Past EndDate
        Given I have valid merchant credentials
		When I use HMAC and GET /batches/totals?EndDate=2012-12-12
        Then response code should be 200
		And response body should contain "count":0
		And response body should contain "net":0
		And response body should contain "volume":0

	
	
	
   