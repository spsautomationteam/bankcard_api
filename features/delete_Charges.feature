@Charges_Delete
Feature: Void an Existing Charge Transaction
	As an API consumer
	I want to Void an Existing Charge Transaction

	@Delete_Charges-Sale_Auth_ForceTransactions
	Scenario Outline: Perform a transaction and then delete that respective transaction 
		Given I have valid merchant credentials
		And I perform a <TypeOfTransaction> transaction
		When I Delete the respective transaction by reference Number
		Then response code should be 200
		Examples:
		|	TypeOfTransaction	|
		|	Sale				|
		|	Authorization		|
		|	force				|		
		
		
	#  ********************** Negative Scenarios ***********************
		
	@Delete_Charges_byDeleteTransactions_Twice
	Scenario: Try to delete already deleted transaction
		Given I have valid merchant credentials
		And I perform a Sale transaction
		When I Delete the respective transaction by reference Number
		And Again try to delete the same transaction
		Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.		
			
#	@Delete_Charges_byBlankReferenceNumber
#    Scenario: Verify the API Delete_Charges with Blank Reference Number
#        Given I set clientId header to `clientId`
#        And I set apikey header to `clientId`
#        And I set merchantId header to `merchantId`
#        And I set merchantKey header to `merchantKey`
#		And I set authorization header to `authorization`  
#	    When Verify Delete Charges with Blank Reference Number
#	   When I use HMAC and GET /charges/
#        Then response code should be 201
#		And response body path $.message should be Internal Server Error
#		And response body path $.detail should be Please contact support for assistance.
		
	@Delete_Charges_byInvalidReferenceNumber
    Scenario: Verify Delete_Charges with Invalid Reference Number
        Given I have valid merchant credentials  
	#   When Verify Delete Charges with Invalid Reference Number ABCABCABC
		When I use HMAC and GET /charges/ABCABCABC
        Then response code should be 404
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
		
